package database;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InsertData
 */
@WebServlet("/InsertProductData")
public class InsertProductData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertProductData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Attempts to insert data from a form to the gameshop database
		try {
			//Initialize the database
			Connection con = ProductDataService.initializeDatabase();
			
			//Create a SQL query to insert data into the Product table
			//Product table consists of four columns, so four '?' are used
			PreparedStatement st = con.prepareStatement("INSERT INTO products VALUES(?, ?, ?, ?)");
			
			//For the first parameter,
			//get the data using request object
			//sets the data to st pointer
			st.setInt(1,  Integer.valueOf(request.getParameter("product_id")));
			
			//same for second parameter
			st.setString(2,  request.getParameter("product_name"));
			
			//third parameter
			st.setFloat(3,  Float.valueOf(request.getParameter("product_price")));
			
			//fourth parameter
			st.setInt(4, Integer.valueOf(request.getParameter("product_quantity")));
			
			//Execute the insert command using executeUpdate()
			//to make changes in database
			st.executeUpdate();
			
			//Close all the connections
			st.close();
			con.close();
			
			//Get a writer pointer
			//to display the successful result
			PrintWriter out = response.getWriter();
			out.println("<html><body><b>Successfully Inserted" + "</b></body></html>");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
