package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import beans.Product;

public class ProductDataService {
	
	//This class is used to initialize the database connection
	protected static Connection initializeDatabase() throws SQLException, ClassNotFoundException {
		
		//Initialize all the information regarding
		//Database Connection
		String dbDriver = "com.mysql.jdbc.Driver";
		String dbURL = "jdbc:mysql://localhost:3306/gameshop";
		
		//Database name to access
		String dbName = "gameshop";
		String user = "root";
		String password = "root";
		
		Class.forName(dbDriver);
		Connection con = DriverManager.getConnection(dbURL + dbName, user, password);
		
		return con;
	}
	
	public int insertOneProduct(Product prod) throws SQLException
	{
		
		String dbURL = "jdbc:mysql://localhost:3306/gameshop";
		String user = "root";
		String password = "root";
		
		Connection c = null;
		PreparedStatement stmt = null;
		int rs = 0;
		
		try {
			c = DriverManager.getConnection(dbURL, user, password);
			System.out.println("  ////// Connection is successful "+ dbURL + " user: " + user + " password: " + password + " ///////// ");
			
			// create sql statement
			stmt = c.prepareStatement("INSERT INTO `products` (`product_id`,`product_name`, `product_price`, `product_quantity`) VALUES (?, ?, ?, ?)");
			stmt.setInt(1, prod.getId());
			stmt.setString(2, prod.getName());
			stmt.setFloat(3, prod.getPrice());
			stmt.setInt(4, prod.getQuantity());
			
			//execute statement
			rs = stmt.executeUpdate();
			
		} catch (SQLException e) {
			
			System.out.println("### ERROR --- connecting to database: ");
			e.printStackTrace();
			System.out.println("### END ###");
		} finally {
			//close the connection to db
			stmt.close();
			c.close();
		}
		
		return rs;
	}
	
	public int deleteOneProduct(int id) throws SQLException
	{
		String dbURL = "jdbc:mysql://localhost:3306/gameshop";
		String user = "root";
		String password = "root";
		
		Connection c = null;
		PreparedStatement stmt = null;
		int rs = 0;
		
		try {
			c = DriverManager.getConnection(dbURL, user, password);
			System.out.println("  ////// Connection is successful "+ dbURL + " user: " + user + " password: " + password + " ///////// ");
			
			// create sql statement
			stmt = c.prepareStatement("DELETE FROM `products` WHERE `products`.`product_id` = ?");
			stmt.setInt(1, id);
			
			//execute statement
			rs = stmt.executeUpdate();
			
		} catch (SQLException e) {
			
			System.out.println("### ERROR --- connecting to database: ");
			e.printStackTrace();
			System.out.println("### END ###");
		} finally {
			//close the connection to db
			stmt.close();
			c.close();
		}
		
		return rs;
	}
	
	public ArrayList<Product> readAllProducts() throws SQLException
	{
		String dbURL = "jdbc:mysql://localhost:3306/gameshop";
		String user = "root";
		String password = "root";
		
		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		ArrayList<Product> products = new ArrayList<>();
		
		try {
			c = DriverManager.getConnection(dbURL, user, password);
			System.out.println("  ////// Connection is successful "+ dbURL + " user: " + user + " password: " + password + " ///////// ");
			
			// create sql statement
			stmt = c.createStatement();
			
			//execute statement
			rs = stmt.executeQuery("SELECT * FROM `products`");
			
			//process the rows in the result set
			while(rs.next()) {
				Product newProduct = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getFloat("product_price"), rs.getInt("product_quantity"));
				products.add(newProduct);
				}
			
		} catch (SQLException e) {
			
			System.out.println("### ERROR --- connecting to database: ");
			e.printStackTrace();
			System.out.println("### END ###");
		} finally {
			//close the connection to db
			stmt.close();
			c.close();
		}
		
		return products;
	}
	
	public Product readOneProduct(int id) throws SQLException
	{
		String dbURL = "jdbc:mysql://localhost:3306/gameshop";
		String user = "root";
		String password = "root";
		
		Product newProduct = new Product();
		
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		
		
		try {
			c = DriverManager.getConnection(dbURL, user, password);
			System.out.println("  ////// Connection is successful "+ dbURL + " user: " + user + " password: " + password + " ///////// ");
			
			// create sql statement
			stmt = c.prepareStatement("SELECT * FROM `products` WHERE `product_id` = ?");
			stmt.setInt(1, id);
			
			
			//execute statement
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				newProduct = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getFloat("product_price"), rs.getInt("product_quantity"));
				}
			
		} catch (SQLException e) {
			
			System.out.println("### ERROR --- connecting to database: ");
			e.printStackTrace();
			System.out.println("### END ###");
		} finally {
			//close the connection to db
			stmt.close();
			c.close();
		}
		
		return newProduct;
	}
	
	public int updateOneProduct(int id, Product prod) throws SQLException
	{
		
		String dbURL = "jdbc:mysql://localhost:3306/gameshop";
		String user = "root";
		String password = "root";
		
		Connection c = null;
		PreparedStatement stmt = null;
		int rs = 0;
		
		
		try {
			c = DriverManager.getConnection(dbURL, user, password);
			System.out.println("  ////// Connection is successful "+ dbURL + " user: " + user + " password: " + password + " ///////// ");
			
			// create sql statement
			stmt = c.prepareStatement("UPDATE `products` SET `product_name` = ?, `product_price` = ?, `product_quantity` = ? WHERE `products`.`product_id` = ?");
			stmt.setString(1, prod.getName());
			stmt.setFloat(2, prod.getPrice());
			stmt.setInt(3, prod.getQuantity());
			stmt.setInt(4, id);
			
			//execute statement
			rs = stmt.executeUpdate();
			
			
		} catch (SQLException e) {
			
			System.out.println("### ERROR --- connecting to database: ");
			e.printStackTrace();
			System.out.println("### END ###");
		} finally {
			//close the connection to db
			stmt.close();
			c.close();
		}
		
		
		return rs;
	}
	
	
}
