package business;

import javax.ejb.Local;

import beans.Product;

import java.util.List;

@Local
public interface OrdersBusinessInterface {

	public void test();
	public List<Product> getOrders();
	public void setOrders(List<Product> orders);
	public Product readOne(int id);
}
