package business;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import beans.Product;

@RequestScoped
@Path("/orders")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class OrdersRestService {
	
	@Inject
	OrdersBusinessInterface bs;
	
	
	///  JSON   /// 
	//get all records and display in json format
	@GET
	@Path("/getjson")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getAllOrdersAsJson()
	{
		return bs.getOrders();
	}
	
	//get all records and display in json format
	@GET
	@Path("/getjsonbyid/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Product getOrderAsJson( @PathParam("id") int id )
	{
		return bs.readOne(id);
	}
	
	///  XML  ///
	//get all records and display in xml format
		@GET
		@Path("/getxml")
		@Produces(MediaType.APPLICATION_XML)
		public Product[] getAllOrdersAsXml()
		{
			List<Product> orders = bs.getOrders();
			return orders.toArray(new Product[orders.size()]);
		}
}
