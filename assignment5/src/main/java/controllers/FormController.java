package controllers;

import java.sql.SQLException;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import beans.Product;
import beans.User;
import business.MyTimerService;
import business.OrdersBusinessInterface;
import database.ProductDataService;

@ManagedBean
@Named
public class FormController {

	@Inject
	OrdersBusinessInterface services;
	
	@EJB
	MyTimerService timer;
	
	public String onSubmit()
	{
		
		
		//get the user values from the input form
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		
		//Prints message to console to tell alternative state
		services.test();
		
		//Start a timer when login is clicked
		//timer.setTimer(5000);
		
		//put the user object into the POST request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		
		//show next page
		return "Home.xhtml";
	}
	
	public OrdersBusinessInterface getService()
	{
		return services;
	}
	

	public String onProductSubmit() {
		
		//get the product values from the input form
				FacesContext context = FacesContext.getCurrentInstance();
				Product product = context.getApplication().evaluateExpressionGet(context, "#{product}", Product.class);
				
				//put the user object into the POST request
				//FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("product", product);
				
				ProductDataService service = new ProductDataService();
				
				try {
					
					service.insertOneProduct(product);
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		
		return "ProductRegistration.xhtml";
	}
	
	public String onDeleteSubmit() {
		
		//get the product values from the input form
				FacesContext context = FacesContext.getCurrentInstance();
				Product product = context.getApplication().evaluateExpressionGet(context, "#{product}", Product.class);
				
				//put the user object into the POST request
				//FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("product", product);
				
				ProductDataService service = new ProductDataService();
				
				try {
					
					service.deleteOneProduct(product.getId());
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		
		return "Home.xhtml";
	}

	public String onUpdateSubmit() {
	
	//get the product values from the input form
			FacesContext context = FacesContext.getCurrentInstance();
			Product product = context.getApplication().evaluateExpressionGet(context, "#{product}", Product.class);
			
			//put the user object into the POST request
			//FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("product", product);
			
			ProductDataService service = new ProductDataService();
			
			try {
				
				service.updateOneProduct(product.getId(), product);
				System.out.println("Updated");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
	
	return "Home.xhtml";
	}
}
