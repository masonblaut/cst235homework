package business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import beans.Product;
import database.ProductDataService;

@Stateless
@Local(OrdersBusinessInterface.class)
@Alternative
public class OrdersBusinessService implements OrdersBusinessInterface {

	List<Product> orders = new ArrayList<Product>();
	
	@Override
	public void test() {
		System.out.println("\n ### This application is running state 1.");

	}
	
	public OrdersBusinessService() throws SQLException
	{
		
		ProductDataService pds = new ProductDataService();
		
		orders = pds.readAllProducts();
	
	}

	@Override
	public List<Product> getOrders() {
		return orders;
	}

	@Override
	public void setOrders(List<Product> orders) {
		this.orders = orders;
	}

}
