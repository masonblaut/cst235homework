package beans;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
 
import javax.faces.bean.ManagedBean;

import database.ProductDataService;

@ManagedBean
public class Orders {
	List <Product> orders = new ArrayList<Product>();
	
	public Orders() throws SQLException {
		ProductDataService pds = new ProductDataService();
		
		orders = pds.readAllProducts();
		
	}

	public List<Product> getOrders() {
		return orders;
	}

	public void setOrders(List<Product> orders) {
		this.orders = orders;
	}
	
	
}
